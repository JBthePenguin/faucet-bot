# Faucet bot

Connect, play lottery and disconnect on each faucet every one hour.  
List of faucets:
* [free bitcoin](https://freebitcoin.io/?ref=443785)
* [free bnb](https://freebinancecoin.com/?ref=117600)
* [free cardano](https://freecardano.com/?ref=296446)
* [free dash](https://freedash.io/?ref=96862)
* [free ethereum](https://freeethereum.com/?ref=161641)
* [free link](https://freechainlink.io/?ref=59904)
* [free neo](https://freeneo.io/?ref=73622)
* [free steam](https://freesteam.io/?ref=102507)
* [free tron](https://free-tron.com/?ref=167607)
* [free usdc](https://freeusdcoin.com/?ref=108534)
* [free usdt](https://freetether.com/?ref=154073)
* [free xem](https://freenem.com/?ref=275164)
* [free xrp](https://coinfaucet.io/?ref=763362)

## Prerequisite
You'll need an account on each faucet in the list above using the same email address.
To create these accounts, you can use my referrals links present in the list above. It doesn't change anything for you and may earn me some extra coins. Thank you in advance for that !!!  
Don't forget to click on the confirmation link sent to your email address (check spam if necessary) for each account creation to activate it.

## Install
* Download and install [geckodriver](https://github.com/mozilla/geckodriver) for your distro.
* Clone repository, create and activate a virtual environment:
```shell
$ git clone https://gitlab.com/JBthePenguin/faucet-bot.git
$ cd faucet-bot
$ virtualenv -p python3 env
$ source env/bin/activate
```
* Install requirements: [selenium](https://www.seleniumhq.org/docs/), [dotenv](https://pypi.org/project/python-dotenv/).
```shell
(env)$ pip install -r requirements.txt
```

* Create a *.env* file at the root directory with all passwords used for login on faucets.  
faucet-bot/.env
```shell
user=youruser@email.com
btc=your_btc_password
bnb=your_bnb_password
ada=your_cardano_password
dash=your_dash_password
eth=your_eth_password
link=your_link_password
neo=your_neo_password
steam=your_steam_password
trx=your_tron_password
usdc=your_usdc_password
usdt=your_usdt_password
xem=your_xem_password
xrp=your_xrp_password
```

## Use
All faucets should be ready to roll before starting the bot.
* Run bot:
```shell
(env)$ python faucet_bot.py
```
