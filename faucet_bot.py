from os import getenv, path
from dotenv import load_dotenv
from selenium.webdriver import Firefox
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementClickInterceptedException, UnexpectedAlertPresentException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
import time

load_dotenv()
# user's email
USER = getenv("user")
# All faucets: {crypto: [faucet url, password, min withdaw, amount_str], ...}
FAUCETS = {
    "btc": ["https://freebitcoin.io", getenv("btc"), 0.0002, "0"],
    "bnb": ["https://freebinancecoin.com", getenv("bnb"), 0.05, "0"],
    "ada": ["https://freecardano.com", getenv("ada"), 5.0, "0"],
    "dash": ["https://freedash.io", getenv("dash"), 0.01, "0"],
    "eth": ["https://freeethereum.com", getenv("eth"), 0.005, "0"],
    "link": ["https://freechainlink.io", getenv("link"), 0.3, "0"],
    "neo": ["https://freeneo.io", getenv("neo"), 1.0, "0"],
    "steam": ["https://freesteam.io", getenv("steam"), 5.0, "0"],
    "trx": ["https://free-tron.com", getenv("trx"), 30.0, "0"],
    "usdc": ["https://freeusdcoin.com", getenv("usdc"), 5.0, "0"],
    "usdt": ["https://freetether.com", getenv("usdt"), 5.0, "0"],
    "xem": ["https://freenem.com", getenv("xem"), 5.0, "0"],
    "xrp": ["https://coinfaucet.io", getenv("xrp"), 5.0, "0"]
}


class Browser(Firefox):

    def connect_to_faucet(self, coin, faucet):
        """Connect to faucet's account"""
        # connect
        try:
        	self.get(faucet[0])
        except TimeoutException:
            print("Timed out waiting for index page to load:", coin)
            return False
        try:
            # email_input = self.find_element_by_xpath(
            #     '//input[@placeholder="Email Address"]')
            email_input = self.find_element_by_class_name("email")
            email_input.send_keys(USER)
            # password_input = self.find_element_by_xpath(
            #     '//input[@placeholder="Password"]')
            password_input = self.find_element_by_class_name("password")
            password_input.send_keys(faucet[1])
        except NoSuchElementException:
            print("Element not founded in login form:", coin)
            return False
        try:
            login_button = self.find_element_by_class_name("login")
            login_button.click()
        except ElementClickInterceptedException:
            print("Login button not founded in login form:", coin)
            return False
        try:
            element_present = EC.presence_of_element_located(
                (By.CLASS_NAME, 'free'))
            WebDriverWait(self, 20).until(element_present)
            return True
        except TimeoutException:
            print("Timed out waiting to load login:", coin)
            return False

    def roll_faucet(self, coin):
        """Play to faucet's lottery"""
        # zoom out to display roll button
        self.execute_script(
            'document.body.style.MozTransform = "scale(0.3)";')
        time.sleep(3)
        # roll
        try:
            roll_button = self.find_element_by_class_name("roll-button")
            roll_button.click()
        except ElementClickInterceptedException:
            print("Roll button not founded in roll page:", coin)
            return False
        try:
            element_present = EC.presence_of_element_located(
                (By.CLASS_NAME, 'popup-close'))
            WebDriverWait(self, 20).until(element_present)
            self.find_element_by_class_name("popup-close").click()
            time.sleep(2)
            # get amount and update it in FAUCETS
            FAUCETS[coin][3] = self.find_element_by_xpath(
                '//a[@href="#"]').text
            return True
        except TimeoutException:
            print("Timed out waiting for page to load: roll", coin)
            return False
        except UnexpectedAlertPresentException:
            print("Alert you are not a robot when rolling", coin)
            return False
        

    def disconnect_faucet(self, coin):
        """Disconnect on faucet."""
        # logout
        try:
            logout_button = self.find_element_by_xpath(
                '//a[@href="/logout"]')
            logout_button.click()
        except ElementClickInterceptedException:
            print("Logout button not founded in roll page:", coin)
            return False
        try:
            element_present = EC.presence_of_element_located(
                (By.CLASS_NAME, 'register-login'))
            WebDriverWait(self, 20).until(element_present)
        except TimeoutException:
            print("Timed out waiting to load: disconnect", coin)


def main():
    """Init browser and play lottery on each faucet every one hour."""
    options = Options()
    options.add_argument("--headless")
    while True:
        # init browser with uBlock addon and maximize window
        browser = Browser(options=options)
        browser.install_addon(path.abspath("uBlock.xpi"), temporary=True)
        browser.set_window_size(1600, 900)
        time.sleep(3)
        # connect, play lottery and disconnect on each faucet
        for key, value in FAUCETS.items():
            login_status = browser.connect_to_faucet(key, value)
            if login_status:
                roll_status = browser.roll_faucet(key)
                if roll_status:
                    browser.disconnect_faucet(key)
        browser.close()
        # check and display if withdraw is possible on each faucet
        for key, value in FAUCETS.items():
            withdraw = "No"
            if float(value[3].split(" ")[0]) > value[2]:
                withdraw = "Yes"
            print(
                value[3], ", min withdraw:", str(value[2]),
                ", withdraw->", withdraw)
        print("---------------------")
        # timer: wait one hour to try again and display counter
        t = 3602  # one hour and two seconds
        while t:
            mins, secs = divmod(t, 60)
            print(f"{mins}m{secs}", end="\r")
            time.sleep(1)
            t -= 1


main()

